package com.ts.demo2;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloController {

	@RequestMapping("/hello")
	public String hello() {
		return "Hello EveryOne, You are in Hello Controller.";
	}
	@RequestMapping("/hi")
	public String hi() {
		return "Hello , You are in Hi Controller. checking";
	}
	@RequestMapping("/abc")
	public String abc() {
		return "Hello , we  are in abc Controller.  checking";
	}
	
	@RequestMapping("/abcd")
	public String abcd() {
		return "Hello , we  are in abcde Controller.  checking";
	}
	
}